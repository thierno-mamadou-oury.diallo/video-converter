# VIDEO CONVERTER

Solution cloud retenue pour le stockage 


## Gloud cloud storage
- La prise en main très facile et intuitive avec l'interface graphique de google
-La création de compte rapide par la synchronisation avec le compte mail personnel
- La documentation compréhensible et bien détaillée
- Le stockage de gros fichier 4 fois plus rapide que amazon S3 et les petits morceaux, 20 fois plus rapide que amazon S3
- Le coût de stockage des objets moins chère ```(sauvegarde et backup en simple ou multi-region)``` de 35% par rapport à amazon

#### Le fichier ```TPPAAS-ee7110ae8016.json``` est à injecter en variable d'environnement
#### Exemple

```
export GOOGLE_APPLICATION_CREDENTIALS=/Users/macbookpro/Documents/Cours_I1-I2-I3/ING3/Conception-systemes-distribues/Tp/TPPAAS-ee7110ae8016.json
```
## Utils
[AWS-S3 vs GCS](https://www.quora.com/Which-one-is-better-Google-Cloud-Storage-or-Amazon-S3)