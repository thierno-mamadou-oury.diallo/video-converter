package edu.esipe.i3.ezipflix.frontend.data.services;

import com.google.api.core.ApiFuture;
import com.google.api.core.ApiFutureCallback;
import com.google.api.core.ApiFutures;
import com.google.api.gax.rpc.ApiException;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.ServiceOptions;
import com.google.cloud.pubsub.v1.Publisher;
import com.google.cloud.storage.*;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.protobuf.ByteString;
import com.google.pubsub.v1.ProjectTopicName;
import com.google.pubsub.v1.PubsubMessage;
import edu.esipe.i3.ezipflix.frontend.ConversionRequest;
import edu.esipe.i3.ezipflix.frontend.ConversionResponse;
import edu.esipe.i3.ezipflix.frontend.data.entities.VideoConversions;
import edu.esipe.i3.ezipflix.frontend.data.repositories.VideoConversionRepository;
import org.apache.commons.fileupload.FileItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

/**
 * Created by Gilles GIRAUD gil on 11/4/17.
 */
@Service
public class VideoConversion {

    @Autowired VideoConversionRepository videoConversionRepository;

    public void messagePub (ConversionRequest request, ConversionResponse response) throws Exception {
        String projectId = ServiceOptions.getDefaultProjectId();
        final VideoConversions conversion = new VideoConversions(
                response.getUuid().toString(),
                request.getPath().toString(),
                "");

        //Always keep MongoDb saving, but i use datastore from google
        videoConversionRepository.save(conversion);
        //Store data
        DataStore dataStore = new DataStore(conversion);
        dataStore.store();
        //retrieve data from store
        dataStore.retrieve();

        ProjectTopicName topic = ProjectTopicName.of(projectId, "video_status");
        Publisher publisher = null;
        try {

            final String message = conversion.toJson();
            publisher = Publisher.newBuilder(topic).build();
            ByteString data = ByteString.copyFromUtf8(message);

            PubsubMessage pubsubMessage = PubsubMessage.newBuilder().setData(data).build();
            ApiFuture<String> messageIdFuture = publisher.publish(pubsubMessage);

            // Add an asynchronous callback to handle success / failure
            ApiFutures.addCallback(
                    messageIdFuture,
                    new ApiFutureCallback<String>() {

                        @Override
                        public void onFailure(Throwable throwable) {
                            if (throwable instanceof ApiException) {
                                ApiException apiException = ((ApiException) throwable);
                                // details on the API exception
                                System.out.println(apiException.getStatusCode().getCode());
                                System.out.println(apiException.isRetryable());
                            }
                            System.out.println("Error publishing message : " + message);
                        }

                        @Override
                        public void onSuccess(String messageId) {
                            // Once published, returns server-assigned message ids (unique within the topic)
                            System.out.println(messageId);
                        }
                    },
                    MoreExecutors.directExecutor());

        } finally {
            if (publisher != null) {
                publisher.shutdown();
                publisher.awaitTermination(1, TimeUnit.MINUTES);
            }
        }


        //Put video to convert in google clous storage

        Storage storage = StorageOptions.newBuilder()
            .setCredentials(ServiceAccountCredentials.fromStream(new FileInputStream("/Users/macbookpro/Documents/Cours_I1-I2-I3/ING3/Conception-systemes-distribues/Tp/TPPAAS-ee7110ae8016.json")))
            .build()
            .getService();

        //Upload a blob to the newly created bucket
        String targetFileStr ="";
        FileItem fileName = null;
        fileName.write (new File("/Users/macbookpro/Documents/Cours_I1-I2-I3/ING3/Conception-systemes-distribues/Tp/video-converter/Game.of.Thrones.S07E07.1080p-intro.mkv"));
        targetFileStr = new String(Files.readAllBytes(Paths.get("/Users/macbookpro/Documents/Cours_I1-I2-I3/ING3/Conception-systemes-distribues/Tp/video-converter/Game.of.Thrones.S07E07.1080p-intro.mkv")));

        BlobId blobId = BlobId.of(projectId, "Game.of.Thrones.S07E07.1080p-intro.mkv");
        BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType("application/octet-stream").build();
        Blob blob = storage.create(blobInfo, targetFileStr.getBytes());

    }

}
