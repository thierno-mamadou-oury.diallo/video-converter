package edu.esipe.i3.ezipflix.frontend.data.services;

import com.google.cloud.datastore.*;
import edu.esipe.i3.ezipflix.frontend.data.entities.VideoConversions;

public class DataStore {

    private VideoConversions videoConversions;
    // Instantiates a client
    private Datastore datastore = DatastoreOptions.getDefaultInstance().getService();
    private Entity entity;
    // The kind for the new entity
    private String kind = "status";
    // The name/ID for the new entity
    private String name = "5644004762845184";
    private KeyFactory keyFactory = datastore.newKeyFactory().setKind(kind);
    public DataStore(VideoConversions videoConversions) {
        this.videoConversions = videoConversions;
    }

    public void store(){

        Key key = keyFactory.newKey(name);
        // Prepares the new entity
        entity = Entity.newBuilder(key)
                .set("id", videoConversions.getUuid())
                .set("originPath", videoConversions.getOriginPath())
                .set("targetPath", videoConversions.getOriginPath())
                .build();

        datastore.put(entity);
    }
    public void retrieve(){

        Key key = keyFactory.newKey(name);
        entity = datastore.get(key);
        System.out.println(entity.toString());

    }

}